

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">



  <script >
    function EnableDisableTextBox(chkPassport) {
      var txtPassportNumber = document.getElementById("txtPassportNumber");
      txtPassportNumber.disabled = chkPassport.checked ? false : true;
        if (!txtPassportNumber.disabled) {
          txtPassportNumber.focus();
        }
    }
  </script>
</head>



<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
   <!-- Page Header -->
   <header class="masthead" style="background-image: url('img/a.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Database configuration</h1>
            <h6>Please configure database inorder to continue!</h6>
            
          </div>
        </div>
      </div>
    </div>
  </header>     


   <!-- Main Content -->
   <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
             <form action="" method="POST">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label>Should be start with alphabet</label>
                  <input type="text" class="form-control" placeholder="Database Name" name="db" required data-validation-required-message="Please enter your Database Name.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group col-xs-12 floating-label-form-group controls">
                  <label>Username</label>
                  <input type="tel" class="form-control" placeholder="Username" name="name" id="user" required data-validation-required-message="Please enter your Username.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label>Password</label>
                  <input type="password" class="form-control" placeholder="password" name="pass" required data-validation-required-message="Please enter password">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <br>
              <div class ="control-group">
                <div class="form-group floating-label-form-group controls">
                  <input type="checkbox" id="check" onclick="EnableDisableTextBox(this)" />
                   Dummy content? 
                  <input type="number" name="dummy" id="txtPassportNumber" disabled="disabled" required data-validation-required-message/>
                </div>
              </div>
              <br>
                <div class="form-group">
                  <input type="submit" value=" Submit " name="submit"/>
                </div>
            </form>
          </div>
        </div>
      </div>
      <hr>


   


  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

<?php
if (is_file("configs.php")) {

  echo "<script>
  alert('Configuration already detected , you will be  redirected to index');
  window.location.href='index.php';
  </script>";
  
} else {
if(isset($_POST['submit'])){
  $dummy = $_POST['n'];
   
   $file = fopen("configs.php","w") or die("Unable to Open");
  if(isset($_POST['submit'])) {
    $db = $_POST['db'];
    $username = $_POST['name'];
    $password = $_POST['pass'];
 }
  $wr='
  <?php
  $hostname = "localhost";
  $username = "'.$username.'";
  $password = "'.$password.'";
  $dbname = "'.$db.'";
  ?>'; 
  fwrite($file, $wr);
  fclose($file);

  if($username!="root" and $password!="user0123") {
    echo "<script type = 'text/javascript'>alert('Username/Password is incorrect');</script>";
    unlink('configs.php');
  }
  try {  
    include_once 'configs.php';
    include_once 'functions.php';
    $conn = new PDO("mysql:host=$servername", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    try {
      
        $sql = "DROP DATABASE".$dbname.";";
        $conn->exec($sql);
        echo "<script type= 'text/javascript'>alert('Database already exist, Welcome back');</script>";
    }
    finally {
        $sql = "CREATE DATABASE  ".$dbname.";";
        $conn->exec($sql);
        $conn->exec("use ".$dbname.";");
        $table1="CREATE TABLE spost (
            idn int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title  varchar(50) NOT NULL,
            content text NOT NULL,
            date  varchar(25) NOT NULL) ";
   
       $conn->exec($table1);	 
   
       $table2 = "CREATE TABLE tags (
            tid  int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            tag varchar(100) NOT NULL) ";
   
       $conn->exec($table2);	 		 
   
      
       $table3 = "CREATE TABLE category (
                 cid INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        categories varchar(100) NOT NULL UNIQUE)";

       $conn->exec($table3);  

  


$table4 = "CREATE TABLE relation (
  blog_id INT(11) NOT NULL,
 tag_id INT(11) NOT NULL,
 cat_id INT(11) NOT NULL,
 FOREIGN KEY (blog_id) REFERENCES spost (idn),
 FOREIGN KEY (tag_id) REFERENCES tags (tid),
 FOREIGN KEY (cat_id) REFERENCES category (cid))";

      $conn->exec($table4);
       
       
        $dummy = $_POST['dummy'];     
        $date = date("Y-m-d H:i:s");
        while($dummy != 0)  {
          $a1 = gen();
          $a2 = gen();
          $a3 = gen();
          $a4 = gen();
          $sql = "INSERT INTO spost (title,content,date) VALUES ('$a1','$a2','$date');";
          $conn->exec($sql);
          $sql = "INSERT INTO tags (tag)  VALUES('$a3');";
          $conn->exec($sql);
          $sql = "INSERT INTO category (categories)  VALUES('$a4')";
          $conn->exec($sql);

          $sql= "INSERT INTO relation (blog_id, tag_id, cat_id)
          SELECT bd.idn, tt.tid, cc.cid FROM spost bd
          JOIN  tags tt ON bd.title = '$a1' AND tt.tag = '$a3'
          JOIN category cc ON bd.title = '$a1' AND cc.categories = '$a4'";
          $conn->exec($sql);
          $dummy--;
        }
        echo "<script>
        alert('Successfully Created! Welcome');
        window.location.href='index.php';
        </script>";   
    }    
} catch(PDOException $e) {
    $conn->rollback();
    echo "Error: " . $e->getMessage();
  }
}
}
?>