<?php
include_once 'functions.php';
try {
  $pdo = connect();
  $sql = "SELECT categories FROM category";         
  $q = $pdo->query($sql);
  $q->setFetchMode(PDO::FETCH_ASSOC);
  $row = $q->fetch();
  $val = $row["categories"];
} catch (Exception $q) {
    die("Could not connect to the database $dbname :" . $e->getMessage());
    } 
if(isset($_POST['submit'])){ 
    $x = $_POST["category"];
    $sql = "INSERT INTO category (categories)  VALUES('$x')";
    $pdo->exec($sql);
    echo "<script type= 'text/javascript'>alert('1 Category added');</script>";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>



  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">


  <style>
    .disabled {
      pointer-events:none;
      opacity:0.0;        
    }
    .ralign {
      position: absolute;
      right: 0px;
    }
    </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add-blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">Add category here!</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
      <div class="post-preview">
      <div class="dropdown">
      <?php
      $stmt3 = $pdo->prepare("SELECT cid,categories FROM category");
      $stmt3->execute();
      while ($row3 = $stmt3->fetch()) {
        $x = $row3['cid'];
        echo '<a href = "tag.php?tag='.$row3['cid'].'">'.$row3['categories'].'</a>';
        echo '<a href="edit.php?id='.$x.'">--EDIT /   </a> ';
        // echo '<a href="a.php?id='.$x.'">--DELETE /   </a> ';
        echo"<a href=\"a.php?id=$x\" OnClick=\"return confirm('Are you sure ?You cannot delete the category if it is used on another post');\"> DELETE </a>";
         ?>
        
      <?php
      echo '<hr>';}
        ?>
        <form action="" method="POST">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label>New category</label>
                  <input type="text" class="form-control" placeholder="New Category" name="category" required data-validation-required-message="Please enter your blog Titile.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div id="success"></div>
              <div class="form-group">
              <input type="submit" value=" Submit " name="submit"/>
              </div>
              </form>
          </ul>         
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>


</body>

</html>