<?php

  function connect(){
    require 'configs.php';
    static $conn;
    try {
      if(!isset($conn)) {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
     } 
    } catch(PDOException $ex) {
      echo "An Error occured! " . $ex->getMessage();
  }
}


 function gen() {
    $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
         'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
         'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
         'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
         'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
         'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
         'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
         'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
         'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
         'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
         'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
         'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
         'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
         'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
         'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
         'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
         'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
         'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
         'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
         'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
         'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
         'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
         'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
         'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
         'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
         'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
         'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
         'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
         'vulputate',
   );    
   shuffle($b);
   $rand = $b[0].$b[1];
   return $rand;
 }


 function trimm($str) {
   $a = explode(" ",$str);
   $cont = implode(" ", array_splice($a,0,20));
   return  $cont;
 }
?>






<?php
/////
function insertTag($dbh, $words, $i) {
  $tagsq = "INSERT INTO tags (tag)  VALUES('$words[$i]')";
  $dbh->query($tagsq);
}
///////
function insertRel($dbh, $words, $title, $i) {
  $relquery = "INSERT INTO relation(blog_id, tag_id)
        SELECT bbid.idn, ttid.tid
        FROM spost bbid JOIN	tags ttid
        ON bbid.title = '$title' AND ttid.tag = '$words[$i]'";
        $dbh->query($relquery);
}


///////////////
function fetchBlog($pdo) {
  $query = "SELECT idn,title,content,date FROM spost";
  $s = $pdo->prepare($query);
  return $s;
}

?>