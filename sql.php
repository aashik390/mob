<?php
include_once 'functions.php';
try {
  $pdo = connect();
  $idd = $_GET['id'];
  $sql = "SELECT idn,title,content,date FROM spost WHERE idn =$idd";         
  $q = $pdo->query($sql);
  $q->setFetchMode(PDO::FETCH_ASSOC);
  $row = $q->fetch();
  $val = $row["idn"];
  $stmt2 = $pdo->prepare("SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid=relation.tag_id;");
  $stmt2->execute([$val]);
} catch (PDOException $e) {
    die("Could not connect to the database $dbname :" . $e->getMessage());
    } 
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>


<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          
        </li>
        <li class="nav-item">
          <a class="nav-link" href="add.php">Add-blog</a>
        </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
            <h1><?php
                    echo '
                      <h2 class="post-title">'.$row["title"].'</h2>
                      <h6 class="post-meta">Posted by
                      <a href="#">Start Bootstrap</a>
                      on '.$row["date"].'</h6>
                      ';
                    echo "Tags:";
                    while ($row2 = $stmt2->fetch()) {
                      echo '<a href = "tag.php?tag='.$row2['tid'].'">'.$row2['tag'].",".'</a>';
                    } 
                    $stmt3 = $pdo->prepare("SELECT category.categories,category.cid FROM relation,category WHERE relation.blog_id = ? AND category.cid=relation.cat_id");
                  $stmt3->execute([$val]);
                  echo "Category:";
                  while ($row3 = $stmt3->fetch()) {
                    echo '<a href = "tag.php?tag='.$row3['cid'].'">'.$row3['categories'].",".'</a>';
                  }
                    echo'<hr>';
                    ?>
                    </h1>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
                 <?php
                    echo '
                      <p class="post-title">'.$row["content"].'</p>
                      <a href="update.php?id='.$idd.'">EDIT    /   </a> 
                      <a href="category.php?id='.$idd.'" >DELETE </a>
                      ';
                  ?>
        </div>
      </div>
    </div>
  </article>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
